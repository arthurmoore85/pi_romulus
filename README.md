# Pi Romulus

Development of Pi Romulus is done [at GitLab](https://gitlab.com/arthurmoore85/pi_romulus).**

**Please note that Pi Romulus no longer supports Python 2.7**

**This repository will be kept up to date with releases from Pi Romulus, but only with releases and not during active development. For development versions, please visit the Gitlab page.**

Retropie ROM downloader (v.3.0.0)

Based on Romulus, the Linux Retropie ROM manager, Pi Romulus is intended to fill a gaping hole
in the Retropie functionality.
It allows you to search for games for the Retropie that you already own and then downloads it
directly to your Retropie installation, no further work required.
What makes Pi Romulus so attractive, is that there is no need for any other computer system.
You dont need to switch on your laptop to download and transfer the games. Just hook up a
keyboard to your Retropie, or ssh into the Pi, search for the game, select and play.

Features:
* Searching ROMs (uses RomDownload)
* Automatic detection of required emulator
* Automatic ROM extraction, if ROM arrives in an archive
* Places ROMs in the correct folder for direct playability
* Download notification
* Filter results per system

Technical Details
-----------------
Pi Romulus is written using Python 3.6.
For it's GUI framework it makes use of the excellent npyscreen library.

## Retropie / Raspbian Buster / Raspberry OS installation
As of version 3.0.0, Pi Romulus no longer supports Python 2.7.
Retropie is based on Raspberry OS which uses Python 2.7 by default, but also includes
Python 3. The following installation instructions are for operating systems that do not
have Python 3 as their default Python version, like RetroPie.
In order to check what version of Python you have installed by default if you do not know,
run the following command in a terminal:

`python --version`

If the version you have starts with a 2, then please check if you have Python 3 installed, by running
the following in the terminal:

`python3`

If this returns an error, please ensure you install Python 3 by following instructions for your OS.
However, if this enters a shell, exit the shell by pressing `CTRL` + `D` and follow the instructions below:

- git clone https://github.com/ArthurMoore85/pi_romulus
- cd pi_romulus
- sudo apt-get install python-pip libarchive-dev
- sudo pip3 install -r requirements.txt
- python3 romulus.py

## Other OS's

Please ensure you have Python 3 installed.
Run the following command from a shell:

`python --version`

If the version you have installed starts with a 2 (eg. v2.7.2) follow the RetroPie installation instructions.
If the version you have installed starts with a 3 (eg. v3.6.0) follow the following installation instructions:

- git clone https://github.com/ArthurMoore85/pi_romulus
- cd pi_romulus
- sudo apt-get install python-pip libarchive-dev
- sudo pip install -r requirements.txt
- python romulus.py

## Developers

All code is licensed under GNU Public License 2 (GPLv2). This license allows you to copy, edit, and redistribute without restriction, as long as it retains the free GPLv2 license.

All help is appreciated, whether filing bug reports, squashing bugs, requesting features or anything else, simply clone this repo, and if you have improved it somehow, make a pull request.

## Reporting bugs

If you have discovered a bug, please report it using the issues tab at the top of the project page.
Before reporting a bug, check if the bug you encounter hasn't already been raised.
You can also reach out to me directly.

## Authors

Arthur Moore <arthur.moore85@gmail.com>
